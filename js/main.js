var ball = document.getElementById('ball');

/*Reset*/
function Reset() {
    ball.className = 'ball';
}

/*Move*/
var MoveBtn = document.getElementById('MoveBtn');

function Move() {
    Reset();
    var MoveDuration = 4000;
    ball.classList.add('Move', 'rotate', 'transition');
    if(document.querySelector('html').classList.contains('safari')){
        setTimeout(function(){ ball.classList.add('MoveEnd' ); ball.classList.remove('Move', 'rotate', 'transition' ) }, MoveDuration);
    } else {
        setTimeout(function(){ ball.classList.add('MoveEnd' ); ball.classList.remove('Move', 'rotate' ) }, MoveDuration);
    }
}

MoveBtn.addEventListener("click", Move);


/*MoveComeBack*/
var MoveComeBackBtn = document.getElementById('MoveComeBackBtn');

function MoveComeBack() {
    Reset();
    var MoveComeBackDuration = 4000;
    ball.classList.add('MoveComeBack', 'rotate', 'transition');
    setTimeout(function(){ ball.classList.remove('MoveComeBack', 'rotate' ); }, MoveComeBackDuration);
}

MoveComeBackBtn.addEventListener("click", MoveComeBack);

/*MoveComebackCircle*/
var MoveComeBackCircleBtn = document.getElementById('MoveComeBackCircleBtn');

function MoveComeBackCircle() {
    Reset();
    var MoveComeBackCircleDuration = 4000;
    ball.classList.add('MoveComeBackCircle', 'rotate', 'transition');
    setTimeout(function(){ ball.classList.remove('Move', 'rotate' ) }, MoveComeBackCircleDuration);
    
}

MoveComeBackCircleBtn.addEventListener("click", MoveComeBackCircle);


/*MoveAndJump*/
var MoveAndJumpBtn = document.getElementById('MoveAndJumpBtn');

function MoveAndJump() {
    Reset();
    var MoveAndJumpDuration = 4000;
    ball.classList.add('MoveAndJump', 'rotate', 'transition');
    if(document.querySelector('html').classList.contains('safari')){
        setTimeout(function(){ ball.classList.add('MoveAndJumpEnd' ); ball.classList.remove('MoveAndJump', 'rotate', 'transition' ) }, MoveAndJumpDuration);
    } else {
        setTimeout(function(){ ball.classList.add('MoveAndJumpEnd' ); ball.classList.remove('MoveAndJump', 'rotate' ) }, MoveAndJumpDuration);
    }
}

MoveAndJumpBtn.addEventListener("click", MoveAndJump);


/* Control Buttons */
/* Move Right and Left */
var MoveRightBtn = document.getElementById('MoveRightBtn');
var MoveLeftBtn = document.getElementById('MoveLeftBtn');


var xPos = 0;
var yPos = 0;
var ballStepDisctance = 100;
yPosition = 0;

function MoveRight() {
    Reset();
    xPos++;
    xPosition = xPos * ballStepDisctance ;
    ball.style.left = xPosition + 'px';
    
}

function MoveLeft(){
    Reset();
    xPos--;
    xPosition = xPos * ballStepDisctance;
    ball.style.left = xPosition + 'px';
}

MoveRightBtn.addEventListener("click", MoveRight);
MoveLeftBtn.addEventListener("click", MoveLeft);

/* Move Top and Bottom */
var MoveTopBtn = document.getElementById('MoveTopBtn');
var MoveBottomBtn = document.getElementById('MoveBottomBtn');


function MoveTop() {
    Reset();
    yPos--;
    yPosition = yPos * ballStepDisctance ;
    ball.style.top = yPosition + 'px';
}

function MoveBottom(){
    Reset();
    yPos++;
    yPosition = yPos * ballStepDisctance;
    ball.style.top = yPosition  + 'px';
}

MoveTopBtn.addEventListener("click", MoveTop);
MoveBottomBtn.addEventListener("click", MoveBottom);

//keyboard controls
function keyboard(event) {
    var key = event.keyCode;
    if (key == 37) {  // 37 is the Left key
        MoveLeft();
    } else if (key == 39 ){ // 39 is the Right key
        MoveRight();
    } else if (key == 38) { // 38 is the Top key
        MoveTop();
    } else if (key == 40) { // 40 is the Bottom key
        MoveBottom();
    }
}

document.addEventListener("keydown", function (event) {
    keyboard(event);
});







